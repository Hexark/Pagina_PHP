<?php
session_start();
require('bda.php');
include('funciones.php');
 ?>
 <!DOCTYPE html>
 <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
 	<head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title>Pentagon</title>
 	<meta name="viewport" content="width=device-width, initial-scale=1">
 	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
 	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
 	<meta name="author" content="FREEHTML5.CO" />
 	<meta property="og:title" content=""/>
 	<meta property="og:image" content=""/>
 	<meta property="og:url" content=""/>
 	<meta property="og:site_name" content=""/>
 	<meta property="og:description" content=""/>
 	<meta name="twitter:title" content="" />
 	<meta name="twitter:image" content="" />
 	<meta name="twitter:url" content="" />
 	<meta name="twitter:card" content="" />

 	<link rel="shortcut icon" href="favicon.ico">



 	<link rel="stylesheet" href="css/animate.css">

 	<link rel="stylesheet" href="css/icomoon.css">

 	<link rel="stylesheet" href="css/bootstrap.css">

 	<link rel="stylesheet" href="css/superfish.css">

 	<link rel="stylesheet" href="css/style.css">



 	<script src="js/modernizr-2.6.2.min.js"></script>

 	</head>
<body>
  <div id="fh5co-header">
  			<header id="fh5co-header-section">
  				<div class="container">
  					<div class="nav-header">
  						<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
  						<h1 id="fh5co-logo"><a href="index.php">Pentagon</a></h1>
              <h1 id="fh5co-logo2"><a href="index.php">P</a></h1>
  						<!-- START #fh5co-menu-wrap -->
  						<nav id="fh5co-menu-wrap" role="navigation">
  							<ul class="sf-menu" id="fh5co-primary-menu">
  								<li class="active">
  									<a href="index.php">Inicio</a>
  								</li>
  								<li>
  									<a href="proyectos.php" class="fh5co-sub-ddown">Proyectos</a>
  								</li>
  								<li><a href="noticias.php">Noticias</a></li>
  								<li><a href="contacto.php">Contacto</a></li>
  								<?php
  							   if(isset($_SESSION['usuario'])){
  										print "<li><a>" . $_SESSION['nombre'] . "</a>
  														<ul class='fh5co-sub-menu'>
														  <li><a href='editar_info.php'>Editar información</a></li>
														  <li><a href='sub_fich.php'>Subir Partida</a></li>
														  <li><a href='destroy_session.php'>Cerrar Sesión</a></li>";


                   if($_SESSION['permiso'] == 1){
                      print "<li><a href='adminpanel/login.php'>CPanel</a></li>";
                      }
                      print "</ul>
  													</li>";
  								}
  								else {
  									print "<li><a href='login.php'>Log in</a></li>";
  								}
  								 ?>
  							</ul>
  						</nav>
  					</div>
  				</div>
  			</header>
  </div>
