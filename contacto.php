<?php include 'header.php';?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
				<div class="desc animate-box">
					<h2><strong>Te escuchamos</strong></h2>
					<span>Si tienes alguna sugerencia o duda contacta con nosotros.</span>
				</div>
			</div>
		</div>
		<div id="fh5co-contact" class="animate-box">
			<div class="container">
				<form action="chk_contacto.php" method="post">
					<div class="row">
						<div class="col-md-6">
							<h3 class="section-title"><strong>Nuestra</strong> localización</h3>

							<ul class="contact-info">
								<li><i class="icon-location-pin"></i>Carrer de les Danses 5, Valencia ,Comunitat Valenciana, España</li>
								<li><i class="icon-phone2"></i>+ 1235 2355 98</li>
								<li><i class="icon-mail"></i><a href="#">info@pentagon.com</a></li>
								<li><i class="icon-globe2"></i><a href="index.php">www.pentagon.com</a></li>
							</ul>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" name="cnombre" placeholder="Nombre">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="email" class="form-control" name="cemail" placeholder="Email">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<textarea class="form-control" id="" cols="30" rows="7" name="cmensaje" placeholder="Mensaje"></textarea>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" value="Enviar Mensaje" class="btn btn-primary">
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div id="map" class="fh5co-map"></div>
		<?php include 'footer.php'; ?>
