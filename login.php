<?php include 'header.php';?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
				<div class="desc animate-box">
					<h2>Login</h2>
					<span>Acceda a su area personal</span>
				</div>
			</div>
		</div>
		<!-- end:header-top -->
		<div id="fh5co-contact" class="animate-box">
			<div class="container">
				<form action="chk_login.php" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" name="cusuario" class="form-control" placeholder="Usuario">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
									<input type="password" name="ccontrasenya" class="form-control" placeholder="Contraseña">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" name="cemail" value="Login" class="btn btn-primary">
									</div>
								</div>
								<div class="col-md-6">
									<p>¿Eres nuevo? Haz click <a href="crear_cuenta.php">Aquí</a> para crear una cuenta.</p>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END fh5co-contact
		<div id="map" class="fh5co-map"></div>
 		-->

		<?php include 'footer.php'; ?>
