<?php include 'header.php';?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
				<div class="desc animate-box">
					<h2>Bienvenido</h2>
					<span>¡Únase a nosotros!</span>
				</div>
			</div>
		</div>
		<!-- end:header-top -->
		<div id="fh5co-contact" class="animate-box">
			<div class="container">
				<form action="chk_crear.php" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" name="cusuario" placeholder="Usuario">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="password" class="form-control" name="ccontrasenya" placeholder="Contraseña">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
									<input type="email" class="form-control" name="cemail" placeholder="Email">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input type="text" class="form-control" name="cnombre" placeholder="Nombre">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" value="Crear" class="btn btn-primary">
									</div>
								</div>
								<div class="col-md-6">
									<p>Si ya posees una cuenta haga click <a href="login.php">Aquí</a> para acceder a ella</p>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>

		<?php include 'footer.php'; ?>
