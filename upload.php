<?php include 'header.php';?>
<?php
$user_dir = $_SESSION['usuario'];
$target_dir = "/var/www/html/users/" . $user_dir ."/";
$target_file = $target_dir . basename($_FILES["cfichero"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$fecha="date_format(date(now()),'%Y%m%d')";
$direccion= "/" . "users/" . $_SESSION['usuario'] . "/" . $_FILES['cfichero']["name"];
// Check if file already exists
if (file_exists($target_file)) {
    $res = "Sorry, file already exists.";
    $uploadOk = 0;
}

// Allow certain file formats
if($imageFileType != "sav") {
    $res = "Sorry, only SAV files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    $res = "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["cfichero"]["tmp_name"], $target_file)) {
		$res = "Tu partida ". basename( $_FILES["cfichero"]["name"]). " ha sido guardada.";
		$conexion = conectaDb();
		$consulta = "insert into partida values(0,'" . $_FILES['cfichero']['name'] . "',$fecha,'" . $_SESSION['id'] . "','$direccion')";
		$result = $conexion->prepare($consulta);
		$result->execute();
    } else {
        $res = "Sorry, there was an error uploading your file.";
    }
}
?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
				<div class="desc animate-box">
					<h2>Guardando tu partida</h2>
					<span>. . .</span>
				</div>
			</div>
		</div>
		<!-- end:header-top -->
		<div id="fh5co-contact" class="animate-box">
			<div class="container">
				<form action="upload.php" method="post" enctype="multipart/form-data">
					<div class="row">
						<div class="col-md-6">
							<div class="row">
								</div>
								<div class='col-md-6'>
									<div class='form-group'>
										<p><?php print($res)?></p>
										<p><a href="index.php" > Volver al inicio </a> </p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<?php include 'footer.php'; ?>
