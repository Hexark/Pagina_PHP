<?php include 'header.php';?>

<?php 

$conexion = conectaDb();
$consulta = "select * from vista_proyectos where id = $_GET[id] and url is not null";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$nombre = $fila['nombre'];
$genero = $fila['genero'];
$plataforma = $fila['plataforma'];
$descripcion=$fila['descripcion'];
$url=$fila['url'];

?>

	<body>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/about-image.jpg);">
				<div class="desc animate-box">
					<h2><strong><?php print($nombre) ?></strong></h2>
					<span><?php print($plataforma) ?></span>
				</div>
			</div>
		</div>
		<div id="fh5co-about">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
						<h3></h3>
						<p><a href="descargas/<?php print($url)?>">Descargar</a></p>
						<p><?php print($descripcion) ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END fh5co-services-section -->
		<?php include 'footer.php'; ?>
