<?php
include  'header.php';
include  'nav.php';
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Añadir usuario</li>
      </ol>
      <div class="row">
        <h1> Añadir usuario</h1>
        <div class="col-12">
          <form action="chk_any_usu.php" method="post">
          <div class="form-group">
            <input class="form-control" name='cusuario' type="text"  placeholder="Usuario">
          </div>
          <div class="form-group">
            <input class="form-control" name='ccontrasenya' type="password" placeholder="Contraseña">
          </div>
          <div class="form-group">
            <input class="form-control" name='cnombre' type="text"  placeholder="Nombre">
          </div>
          <div class="form-group">
            <input class="form-control" name='cemail' type="email"  placeholder="Email">
          </div>
          <div class="form-group">
            <input class="form-control" name='cpermiso' type="text"  placeholder="Permiso"> <p>Adminsitrador = 1 | Usuario = 0<p>
          </div>
          <div class="form-group">
          </div>
            <input class="btn btn-primary" type="submit" value="Enviar" >
            <a class="btn btn-primary" href="usuarios.php">Cancelar</a>          
          </form>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>