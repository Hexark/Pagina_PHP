<?php include 'header.php';
include 'nav.php';
?>
<script>
function abrirenlace(id) {
    var enlace = "../proyecto.php?id=".concat(id);

    window.open(enlace);
}
</script>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Proyectos</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Proyectos <a href='any_pro.php'><i class='fa fa-plus'></i></a></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NOMBRE</th>
                  <th>GENERO</th>
                  <th>PLATAFORMA</th>
                  <th>FECHA ALTA</th>
                  <th>DISPONIBLE</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $conexion = conectaDb();
                    $consulta = "select * from proyectos";
                    $result = $conexion->prepare($consulta);
                    $result->execute();
                    while ($fila = $result->fetch())
                    {
                      $id = $fila['id'];
                      $nombre=$fila['nombre'];
                      $genero=$fila['genero'];
                      $plataforma=$fila['plataforma'];
                      $url=$fila['url'];
                      $fecha_alta=date_format(date_create($fila['fecha_alta']),"Y-m-d");
                      if($url != null){
                        $url = "Disponible";
                      }
                      else{
                        $url = "No Disponible";
                      }
                      print("<tr><td>$id</td>
                      <td>$nombre</td>
                      <td>$genero</td>
                      <td>$plataforma</td>
                      <td>$fecha_alta</td>
                      <td>$url</td>
                      <td><a href='#' onclick='abrirenlace($id)' '><i class='fa fa-eye'></i> </a></td>
                      <td><a href='edit_pro.php?id=$id'><i class='fa fa-pencil'></i></a></td>
                      <td><a href='eli_pro.php?id=$id'><i class='fa fa-trash'></i></a></td></tr>");
                    }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>