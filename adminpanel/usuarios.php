<?php include 'header.php';
include 'nav.php';
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Usuarios</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Usuarios <a href='any_usu.php'><i class='fa fa-plus'></i></a></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>USUARIO</th>
                  <th>NOMBRE</th>
                  <th>EMAIL</th>
                  <th>PERMISOS</th>
                  <th>FECHA ALTA</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $conexion = conectaDb();
                    $consulta = "select * from usuarios";
                    $result = $conexion->prepare($consulta);
                    $result->execute();
                    while ($fila = $result->fetch())
                    {
                      $id = $fila['id'];
                      $usuario=$fila['usuario'];
                      $nombre=$fila['nombre'];
                      $email = $fila['email'];
                      $permiso=$fila['permiso'];
                      $fecha_alta=date_format(date_create($fila['fecha_alta']),"Y-m-d");
                      if($permiso == "1"){
                        $permiso = "Administrador";
                      }
                      else{
                        $permiso = "Usuario";
                      }
                      print("<tr><td>$id</td>
                      <td>$usuario</td>
                      <td>$nombre</td>
                      <td>$email</td>
                      <td>$permiso</td>
                      <td>$fecha_alta</td>
                      <td><a href='ver_usu.php?id=$id'><i class='fa fa-eye'></i> </a></td>
                      <td><a href='edit_usu.php?id=$id'><i class='fa fa-pencil'></i></a></td>
                      <td><a href='eli_usu.php?id=$id'><i class='fa fa-trash'></i></a></td></tr>");
                    }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>