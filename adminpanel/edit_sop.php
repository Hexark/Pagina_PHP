<?php
include  'header.php';
include  'nav.php';
$id=$_GET['id'];

$conexion = conectaDb();
$consulta = "select * from soporte where id = $id";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$id = $fila['id'];
$nombre=$fila['nombre'];
$email=$fila['email'];
$mensaje=$fila['mensaje'];
$solventado = $fila['solventado'];
$fecha=$fila['fecha'];
$subtitulo=$fila['subtitulo'];

?>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<div class="content-wrapper">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="soporte.php">Soporte</a>
      </li>
      <li class="breadcrumb-item active">Editar ticket</li>
    </ol>
    <div class="row">
      <h1> Editar ticket</h1>
      <div class="col-12">
        <form action="chk_edit_sop.php" method="post">
        <div class="form-group">
        <div class="form-group">
          <input class="form-control" name='cid' type="text"  placeholder="<?php print($id)?>" value="<?php print($id)?>" readonly>
        </div>
          <input class="form-control" name='cnombre' type="text"  placeholder="<?php print($nombre)?>" value="<?php print($nombre)?>">
        </div>
        <div class="form-group">
          <input class="form-control" name='csubtitulo' type="text" placeholder="<?php print($subtitulo)?>" value="<?php print($subtitulo)?>">
        </div>
        <div class="form-group">
          <input class="form-control" name='cemail' type="text"  placeholder="<?php print($email)?>" value="<?php print($email)?>">
        </div>
        <textarea id="summernote" name="cmensaje"> <?php print($mensaje)?></textarea>
        <script>
          $('#summernote').summernote({
            placeholder: 'Introduce tu mensaje aquí',
            tabsize: 2,
            height: 200
          });
        </script>
        <div class="form-group">
          <input class="form-control" name='csolventado' type="text"  placeholder="Solventado" value="<?php print($solventado)?>"> <p>Solventado = 1 | No Solventado = 0<p>
        </div>
        <div class="form-group">
        </div>
          <input class="btn btn-primary" type="submit" value="Enviar" >
          <a class="btn btn-primary" href="soporte.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>
<?php include 'footer.php' ?>