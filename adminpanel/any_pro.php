<?php
include  'header.php';
include  'nav.php';
?>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<div class="content-wrapper">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="proyectos.php">Proyectos</a>
      </li>
      <li class="breadcrumb-item active">Añadir Proyecto</li>
    </ol>
    <div class="row">
      <h1> Añadir Proyecto</h1>
      <div class="col-12">
        <form action="chk_any_pro.php" method="post">
        <div class="form-group">
          <input class="form-control" name='cnombre' type="text"  placeholder="Nombre">
        </div>
        <div class="form-group">
          <input class="form-control" name='cgenero' type="text"  placeholder="Genero">
        </div>
        <div class="form-group">
          <input class="form-control" name='cplataforma' type="text" placeholder="Plataforma">
        </div>
        <div class="form-group">
         <p>Introduce una imagen de banner</p>
        </div>
        <div class="form-group">
          <input class="form-control" name='cbanner' type="file" placeholder="Banner">
        </div>
        <div class="form-group">
         <p>Introduce una url para el setup</p>
        </div>
        <div class="form-group">
          <input class="form-control" name='curl' type="file" placeholder="URL">
        </div>
        <textarea id="summernote" name="cdescripcion"></textarea>
        <script>
          $('#summernote').summernote({
            placeholder: 'Introduce tu texto aquí',
            tabsize: 2,
            height: 200
          });
        </script>
        <div class="form-group">
        </div>
          <input class="btn btn-primary" type="submit" value="Enviar" >
          <a class="btn btn-primary" href="noticias.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>
<?php include 'footer.php' ?>