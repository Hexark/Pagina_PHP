<?php include 'header.php';
include 'nav.php';
?>
<script>
function abrirenlace(id) {
    var enlace = "../noticia.php?id=".concat(id);

    window.open(enlace);
}
</script>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Noticias</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Noticias <a href='any_not.php'><i class='fa fa-plus'></i></a></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>TITULO</th>
                  <th>SUBTITULO</th>
                  <th>DISPONIBLE</th>
                  <th>FECHA</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $conexion = conectaDb();
                    $consulta = "select * from noticias";
                    $result = $conexion->prepare($consulta);
                    $result->execute();
                    while ($fila = $result->fetch())
                    {
                      $id = $fila['id'];
                      $titulo=$fila['titulo'];
                      $subtitulo=$fila['subtitulo'];
                      $disponible=$fila['disponible'];
                      $fecha=date_format(date_create($fila['fecha']),"Y-m-d");
                      if($disponible == "1"){
                        $disponible = "Disponible";
                      }
                      else{
                        $disponible = "No Disponible";
                      }
                      print("<tr><td>$id</td>
                      <td>$titulo</td>
                      <td>$subtitulo</td>
                      <td>$disponible</td>
                      <td>$fecha</td>
                      <td><a href='#' onclick='abrirenlace($id)' '><i class='fa fa-eye'></i> </a></td>
                      <td><a href='edit_not.php?id=$id'><i class='fa fa-pencil'></i></a></td>
                      <td><a href='eli_not.php?id=$id'><i class='fa fa-trash'></i></a></td></tr>");
                    }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>