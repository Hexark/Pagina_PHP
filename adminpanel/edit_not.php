<?php
include  'header.php';
include  'nav.php';
$id=$_GET['id'];

$conexion = conectaDb();
$consulta = "select * from noticias where id = $id";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$id = $fila['id'];
$titulo=$fila['titulo'];
$descripcion=$fila['descripcion'];
$imagen=$fila['imagen'];
$disponible = $fila['disponible'];
$fecha=$fila['fecha'];
$subtitulo=$fila['subtitulo'];

?>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<div class="content-wrapper">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="noticias.php">Noticias</a>
      </li>
      <li class="breadcrumb-item active">Editar noticia</li>
    </ol>
    <div class="row">
      <h1> Editar noticia</h1>
      <div class="col-12">
        <form action="chk_edit_not.php" method="post">
        <div class="form-group">
        <div class="form-group">
          <input class="form-control" name='cid' type="text"  placeholder="<?php print($id)?>" value="<?php print($id)?>" readonly>
        </div>
          <input class="form-control" name='ctitulo' type="text"  placeholder="<?php print($titulo)?>" value="<?php print($titulo)?>">
        </div>
        <div class="form-group">
          <input class="form-control" name='csubtitulo' type="text" placeholder="<?php print($subtitulo)?>" value="<?php print($subtitulo)?>">
        </div>
        <div class="form-group">
         <p>Introduce una imagen de banner</p>
        </div>
        <div class="form-group">
          <input class="form-control" name='cimagen' type="file" placeholder="Imagen">
        </div>
        <textarea id="summernote" name="cdescripcion"> <?php print($descripcion)?></textarea>
        <script>
          $('#summernote').summernote({
            placeholder: 'Introduce tu mensaje aquí',
            tabsize: 2,
            height: 200
          });
        </script>
        <div class="form-group">
          <input class="form-control" name='cdisponible' type="text"  placeholder="Disponible" value="<?php print($disponible)?>"> <p>Disponible = 1 | No Disponible = 0<p>
        </div>
        <div class="form-group">
        </div>
          <input class="btn btn-primary" type="submit" value="Enviar" >
          <a class="btn btn-primary" href="noticias.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>
<?php include 'footer.php' ?>