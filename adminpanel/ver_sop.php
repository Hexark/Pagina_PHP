<?php
include  'header.php';
include  'nav.php';

$id=$_GET['id'];

$conexion = conectaDb();
$consulta = "select * from soporte where id = $id";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$id = $fila['id'];
$nombre=$fila['nombre'];
$email=$fila['email'];
$mensaje=$fila['mensaje'];
$solventado = $fila['solventado'];
$fecha=$fila['fecha'];
$subtitulo=$fila['subtitulo'];
?>

<div class="content-wrapper">
  <div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="soporte.php">Soporte</a>
      </li>
      <li class="breadcrumb-item active">Ver ticket</li>
    </ol>
    <div class="row">
      <h1> Ver ticket</h1>
      <div class="col-12">
        <form action="#">
        <div class="form-group">
          <input class="form-control" name='cnombre' type="text"  placeholder="Id" value="<?php print($id)?>" readonly>
        </div>
        <div class="form-group">
          <input class="form-control" name='cnombre' type="text"  placeholder="Nombre" value="<?php print($nombre)?>" readonly>
        </div>
        <div class="form-group">
          <input class="form-control" name='csubtitulo' type="text" placeholder="Subtitulo" value="<?php print($subtitulo)?>" readonly>
        </div>
        <div class="form-group">
          <input class="form-control" name='cemail' type="text"  placeholder="Email" value="<?php print($email)?>" readonly>
        </div>
        <div id="summernote">
        <?php print($mensaje)?>
        </div>
        <div class="form-group">
          <input class="form-control" name='csolventado' type="text"  placeholder="Solventado" value="<?php print($solventado)?>" readonly> <p>Solventado = 1 | No Solventado = 0<p>
        </div>
        <div class="form-group">
        </div>
          <a class="btn btn-primary" href="soporte.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>