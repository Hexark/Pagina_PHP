<?php
include  'header.php';
include  'nav.php';

$id=$_GET['id'];

$conexion = conectaDb();
$consulta = "select * from usuarios where id = $id";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$id = $fila['id'];
$usuario=$fila['usuario'];
$nombre=$fila['nombre'];
$contrasenya=$fila['contrasenya'];
$email = $fila['email'];
$permiso=$fila['permiso'];
                ?>
?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="usuarios.php">Usuarios</a>
        </li>
        <li class="breadcrumb-item active">Editar usuario</li>
      </ol>
      <div class="row">
        <h1> Editar usuario</h1>
        <div class="col-12">
          <form action="chk_edit_usu.php" method="post">
          <div class="form-group">
            <input class="form-control" name='id' type="text" placeholder="<?php print($id)?>" value="<?php print($id)?>" readonly>
          </div>
          <div class="form-group">
            <input class="form-control" name='cusuario' type="text"  placeholder="<?php print($usuario)?>" value="<?php print($usuario)?>">
          </div>
          <div class="form-group">
            <input class="form-control" name='ccontrasenya' type="password" placeholder="<?php print($contrasenya)?>" value="<?php print($contrasenya)?>">
          </div>
          <div class="form-group">
            <input class="form-control" name='cnombre' type="text"  placeholder="<?php print($nombre)?>" value="<?php print($nombre)?>">
          </div>
          <div class="form-group">
            <input class="form-control" name='cemail' type="email"  placeholder="<?php print($email)?>" value="<?php print($email)?>">
          </div>
          <div class="form-group">
            <input class="form-control" name='cpermiso' type="text"  placeholder="<?php print($permiso)?>" value="<?php print($permiso)?>"> <p>Administrador = 1 | Usuario = 0<p>
          </div>
          <div class="form-group">
          </div>
            <input class="btn btn-primary" type="submit" value="Enviar" >
            <a class="btn btn-primary" href="usuarios.php">Cancelar</a>          
          </form>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>