<?php
include  'header.php';
include  'nav.php';
?>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<div class="content-wrapper">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="soporte.php">Soporte</a>
      </li>
      <li class="breadcrumb-item active">Añadir ticket</li>
    </ol>
    <div class="row">
      <h1> Añadir ticket</h1>
      <div class="col-12">
        <form action="chk_any_sop.php" method="post">
        <div class="form-group">
          <input class="form-control" name='cnombre' type="text"  placeholder="Nombre">
        </div>
        <div class="form-group">
          <input class="form-control" name='csubtitulo' type="text" placeholder="Subtitulo">
        </div>
        <div class="form-group">
          <input class="form-control" name='cemail' type="text"  placeholder="Email">
        </div>
        <textarea id="summernote" name="cmensaje"></textarea>
        <script>
          $('#summernote').summernote({
            placeholder: 'Introduce tu texto aquí',
            tabsize: 2,
            height: 200
          });
        </script>
        <div class="form-group">
          <input class="form-control" name='csolventado' type="text"  placeholder="Solventado"> <p>Solventado = 1 | No Solventado = 0<p>
        </div>
        <div class="form-group">
        </div>
          <input class="btn btn-primary" type="submit" value="Enviar" >
          <a class="btn btn-primary" href="soporte.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>
<?php include 'footer.php' ?>