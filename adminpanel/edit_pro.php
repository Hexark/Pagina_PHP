<?php
include  'header.php';
include  'nav.php';
$id=$_GET['id'];

$conexion = conectaDb();
$consulta = "select * from proyectos where id = $id";
$result = $conexion->prepare($consulta);
$result->execute();
$fila = $result->fetch();

$id = $fila['id'];
$nombre=$fila['nombre'];
$descripcion=$fila['descripcion'];
$genero=$fila['genero'];
$plataforma=$fila['plataforma'];
$banner=$fila['banner'];
$url=$fila['url'];


?>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

<div class="content-wrapper">
  <div class="container-fluid">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="proyectos.php">Proyectos</a>
      </li>
      <li class="breadcrumb-item active">Editar Proyecto</li>
    </ol>
    <div class="row">
      <h1> Editar proyecto</h1>
      <div class="col-12">
        <form action="chk_edit_pro.php" method="post">
        <div class="form-group">
        <div class="form-group">
          <input class="form-control" name='cid' type="text"  placeholder="<?php print($id)?>" value="<?php print($id)?>" readonly>
        </div>
          <input class="form-control" name='cnombre' type="text"  placeholder="<?php print($nombre)?>" value="<?php print($nombre)?>">
        </div>
        <div class="form-group">
          <input class="form-control" name='cgenero' type="text" placeholder="<?php print($genero)?>" value="<?php print($genero)?>">
        </div>
        <div class="form-group">
          <input class="form-control" name='cplataforma' type="text" placeholder="<?php print($plataforma)?>" value="<?php print($plataforma)?>">
        </div>
        <div class="form-group">
         <p>Introduce una imagen de banner</p>
        </div>
        <div class="form-group">
          <input class="form-control" name='cbanner' type="file" placeholder="Banner">
        </div>
        <div class="form-group">
         <p>Introduce una URL para el setup</p>
        </div>
        <div class="form-group">
          <input class="form-control" name='curl' type="file" placeholder="URL">
        </div>
        <textarea id="summernote" name="cdescripcion"> <?php print($descripcion)?></textarea>
        <script>
          $('#summernote').summernote({
            placeholder: 'Introduce tu mensaje aquí',
            tabsize: 2,
            height: 200
          });
        </script>
        <div class="form-group">
        </div>
          <input class="btn btn-primary" type="submit" value="Enviar" >
          <a class="btn btn-primary" href="proyectos.php">Cancelar</a>          
        </form>
      </div>
    </div>
  </div>
<?php include 'footer.php' ?>