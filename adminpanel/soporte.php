<?php include 'header.php';
include 'nav.php';
?>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active">Soporte</li>
      </ol>
      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Soporte <a href='any_sop.php'><i class='fa fa-plus'></i></a></div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NOMBRE</th>
                  <th>SUBTITULO</th>
                  <th>EMAIL</th>
                  <th>SOLVENTADO</th>
                  <th>FECHA</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                    $conexion = conectaDb();
                    $consulta = "select * from soporte";
                    $result = $conexion->prepare($consulta);
                    $result->execute();
                    while ($fila = $result->fetch())
                    {
                      $id = $fila['id'];
                      $nombre=$fila['nombre'];
                      $subtitulo=$fila['subtitulo'];
                      $email = $fila['email'];
                      $solventado=$fila['solventado'];
                      $fecha=date_format(date_create($fila['fecha']),"Y-m-d");
                      if($solventado == "1"){
                        $solventado = "Solventado";
                      }
                      else{
                        $solventado = "No Solventado";
                      }
                      print("<tr><td>$id</td>
                      <td>$nombre</td>
                      <td>$subtitulo</td>
                      <td>$email</td>
                      <td>$solventado</td>
                      <td>$fecha</td>
                      <td><a href='ver_sop.php?id=$id'><i class='fa fa-eye'></i> </a></td>
                      <td><a href='edit_sop.php?id=$id'><i class='fa fa-pencil'></i></a></td>
                      <td><a href='eli_sop.php?id=$id'><i class='fa fa-trash'></i></a></td></tr>");
                    }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<?php include 'footer.php' ?>