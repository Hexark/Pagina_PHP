<?php
session_start();
require('bda.php');
include('funciones.php');

unset($_SESSION['id']);
unset($_SESSION['usuario']);
unset($_SESSION['nombre']);
session_destroy();
header("Location: index.php");
?>
