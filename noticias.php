
<?php	include 'header.php'?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/work-3.jpg);">
				<div class="desc animate-box">
					<h2>Nuestro <strong>Blog</strong></h2>
					<span>Estas son todas las noticias.</a></span>
				</div>
			</div>

		</div>
		<!-- end:header-top -->
		<div id="fh5co-blog-section" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<?php
								$conexion = conectaDb();
								$consulta = "select * from vista_fechas where disponible = 1";
								$result = $conexion->prepare($consulta);
								$result->execute();
								while ($fila = $result->fetch())
								{
								 print "<div class='col-lg-4d col-md-4'>
									<div class='fh5co-blog animate-box'>
										<a href='noticia.php?id=$fila[id]'><img class='img-responsive' src='$fila[imagen]' alt=''></a>
										<div class='blog-text'>
											<div class='prod-title'>
												<h3><a href='noticia.php?id=$fila[id]'>$fila[titulo]</a></h3>
												<span class='posted_by'>$fila[dia]/$fila[mes]/$fila[anyo]</span>
												<p>$fila[subtitulo]</p>
											</div>
										</div>
									</div>
								</div>";
								}
					 ?>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
