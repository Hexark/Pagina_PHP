<?php include 'header.php';?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/cover_bg_1.jpg);">
				<div class="desc animate-box">
					<h2><strong>Entretenimiento</strong></h2>
					<span>Trabajamos en ello</a></span>
					<span><a class="btn btn-primary btn-lg" href="contacto.php">Envia tus ideas!</a></span>
				</div>
			</div>
		</div>
		<!-- end:header-top -->
		<div id="fh5co-blog-section" class="fh5co-section-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center heading-section animate-box">
						<h3>Ultimas noticias</h3>
						<p>Estas son las ultimas noticias relacionadas con nuestros juegos o eventos.</p>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row row-bottom-padded-mdD">
					<?php
								$conexion = conectaDb();
								$consulta = "select * from vista_fechas where disponible = 1 limit 3";
								$result = $conexion->prepare($consulta);
								$result->execute();
								while ($fila = $result->fetch())
								{
								 print "<div class='col-lg-4d col-md-4'>
	 								<div class='fh5co-blog animate-box'>
	 									<a href='noticias.php?id=$fila[id]'><img class='img-responsive' src='$fila[imagen]' alt=''></a>
	 									<div class='blog-text'>
	 										<div class='prod-title'>
	 											<h3><a href='noticia.php?id=$fila[id]'>$fila[titulo]</a></h3>
	 											<span class='posted_by'>$fila[dia]/$fila[mes]/$fila[anyo]</span>
	 											<p>$fila[subtitulo]</p>
	 										</div>
	 									</div>
	 								</div>
	 							</div>";
								}
					 ?>
				</div>
			</div>
			<div class="col-md-4 col-md-offset-4 text-center animate-box">
				<a href="noticias.php" class="btn btn-primary btn-lg">Ver todas las noticias</a>
			</div>

		</div>
		<div id="fh5co-portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3 text-center heading-section animate-box">
						<h3>Recien tostado</h3>
						<p>Este es el juego mas reciente desarrollado por nosotros.</p>
						<?php
							$conexion = conectaDb();
							$consulta = "select * from juego_reciente where url is not null";
							$result = $conexion->prepare($consulta);
							$result->execute();
							while ($fila = $result->fetch())
							{
								print"<div class='row row-bottom-padded-md'>
											<div class='col-md-12'>
												<ul id='fh5co-portfolio-list'>
													<li class='two-third animate-box' data-animate-effect='fadeIn' style='background-image:url($fila[banner])'>
														<a href='proyecto.php?id=$fila[id]' class='color-6'>
															<div class='case-studies-summary'>
																<span>$fila[plataforma]</span>
																<h2>$fila[nombre]</h2>
															</div>
														</a>
													</li>
													</ul>
											</div>
										</div>";
							}
						?>
					</div>
				</div>
				<div class="row row-bottom-padded-md">
					<div class="col-md-12">
						<ul id="fh5co-portfolio-list">
							<?php
										$conexion = conectaDb();
										$consulta = "select * from vista_proyectos where url is not null limit 6";
										$result = $conexion->prepare($consulta);
										$result->execute();
										while ($fila = $result->fetch())
										{

											print"<li class='one-third animate-box' data-animate-effect='fadeIn' style='background-image: url($fila[banner]); ''>
												<a href='proyecto.php?id=$fila[id]' class='color-3'>
													<div class='case-studies-summary'>
														<span>$fila[plataforma]</span>
														<h2>$fila[nombre]</h2>
													</div>
												</a>
											</li>";
										}
							?>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center animate-box">
						<a href="proyectos.php" class="btn btn-primary btn-lg">Ver todos nuestros proyectos</a>
					</div>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
