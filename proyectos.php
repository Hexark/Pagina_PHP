<?php include 'header.php';?>
		<div id="fh5co-wrapper">
		<div id="fh5co-page">
		<div class="fh5co-hero fh5co-hero-2">
			<div class="fh5co-overlay"></div>
			<div class="fh5co-cover fh5co-cover_2 text-center" data-stellar-background-ratio="0.5" style="background-image: url(images/blog-2.jpg);">
				<div class="desc animate-box">
					<h2>Nuestros <strong>Proyectos</strong></h2>
					<span>Estos son nuestros proyectos y los que están por venir.</a></span>
				</div>
			</div>
		</div>
		<!-- end:header-top -->

		<div id="fh5co-portfolio">
			<div class="container">


				<div class="row row-bottom-padded-md">
					<div class="col-md-12">
						<ul id="fh5co-portfolio-list">
						<?php
									$conexion = conectaDb();
									$consulta = "select * from proyectos where url is not null";
									$result = $conexion->prepare($consulta);
									$result->execute();
									while ($fila = $result->fetch())
									{
										print"<li class='one-third animate-box' data-animate-effect='fadeIn' style='background-image: url($fila[banner]); ''>
											<a href='proyecto.php?id=$fila[id]' class='color-3'>
												<div class='case-studies-summary'>
													<span>$fila[plataforma]</span>
													<h2>$fila[nombre]</h2>
												</div>
											</a>
										</li>";
									}
						?>
						</ul>
					</div>
				</div>

			</div>
		</div>
		<?php include 'footer.php'; ?>
